import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
    Home, 
    Search, 
    Favorite, 
    Splash, 
    Login, 
    Register, 
    SuccessReg, 
    BookDetail
} from '../pages'
import Icon from 'react-native-vector-icons/dist/Ionicons';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const mainApp = () => {
    return(
        <Tab.Navigator>
            <Tab.Screen 
                name="Home" 
                component={Home} 
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="home" size={size} color={color} />                        
                    )
                }}
            />
            <Tab.Screen 
                name="Search" 
                component={Search} 
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="search" size={size} color={color} />                        
                    )
                }}
            />
            <Tab.Screen 
                name="Favorite" 
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="ios-heart" size={size} color={color} />                        
                    )
                }}
                component={Favorite} 
            />
        </Tab.Navigator>    
    )
}

export class Router extends Component {
    render() {
        return (
            <Stack.Navigator initialRouteName='Splash'>
                <Stack.Screen name="MainApp" component={mainApp} options={{headerShown: false}}/>
                <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}}/>
                <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
                <Stack.Screen name="Register" component={Register} options={{headerShown: false}}/>
                <Stack.Screen name="SuccessReg" component={SuccessReg} options={{headerShown: false}}/>
                <Stack.Screen name="BookDetail" component={BookDetail} options={{headerShown: false}}/>
            </Stack.Navigator>
        )
    }
}

export default Router
