import React, { Component } from 'react'
import {
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native'
import {connect} from 'react-redux'
import {RegisterAction} from '../../redux/actions/RegisterAction'
import {styles} from './style'

export class Register extends Component {
    constructor(){
        super();
        // Mendefinisikan state awal
        this.state = {
          fulName: '',
          email: '',
          password: '',
          isLoading: false
        }
    }

    // Jika register sukses, lanjut ke halaman Success Register
    componentDidUpdate(){
        if (this.props.dataRegister.isRegisterSuccess === true){
            this.props.navigation.navigate('SuccessReg')
        }
    }

    // Menghandle perubahan nilai fullname
    handleChangeName = (fulName) => {
        this.setState({fulName})
    }

    // Menghandle perubahan nilai email
    handleChangeEmail = (email) => {
        this.setState({email})
    }

    // Menghandle perubahan nilai password
    handleChangePassword = (password) => {
    this.setState({password})
    }

    // Menghandle submit item
    handleSubmitItem = () => {
        const submitData = {
            fulName: this.state.fulName,
            email: this.state.email,
            password: this.state.password,
        }
        this.props.RegisterAction(submitData)
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.registerText}>Register Now!</Text>

                {/* Menampilkan pesan error */}
                {this.props.dataRegister.errorMessage !== null ? 
                    <View style={styles.errorView}>
                        <Text style={styles.errorText}>
                            {this.props.dataRegister.errorMessage}
                        </Text> 
                    </View> : null
                }

                <View>
                    {/* Input fullname */}
                    <TextInput 
                        onChangeText={this.handleChangeName} 
                        style={styles.inputData} 
                        placeholder='Masukkan Full Name'
                        placeholderTextColor='grey'
                    /> 
                    {/* Input email */}
                    <TextInput 
                        onChangeText={this.handleChangeEmail} 
                        style={styles.inputData} 
                        placeholder='Masukkan Email'
                        placeholderTextColor='grey'
                    /> 
                    {/* Input password */}
                    <TextInput 
                        onChangeText={this.handleChangePassword} 
                        style={styles.inputData} 
                        placeholder='Masukkan Password'
                        placeholderTextColor='grey'
                        secureTextEntry={true}
                    /> 
                </View>    

                {/* Menampilkan tombol register */}
                <TouchableOpacity style={styles.registerButton} onPress={this.handleSubmitItem} >
                    <Text style={styles.registerButtonText}>Register</Text>
                </TouchableOpacity>

                {/* Menampilkan pilihan login */}
                <Text>Already have an account?</Text>
                <TouchableOpacity style={styles.loginButton}
                    onPress={()=>{
                        this.props.navigation.navigate('Login')
                    }}                                                        >
                    <Text style={styles.loginButtonText}>Login</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    dataRegister: state.RegisterReducer
})
  
const mapDispatchToProps = {
    RegisterAction
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Register)
