import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 40,
    },
    registerText: {
        fontSize: 30,
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 20,
    },
    errorView: {
        marginVertical: 20,
        backgroundColor: 'red',
        padding: 5,
        borderRadius: 5,
        width: 280,
    },
    errorText: {
        fontSize: 15,
        textAlign: 'center',
        color: 'white',
    },
    inputData: {
        padding: 5,
        backgroundColor: 'white',
        color: 'black',
        width: 250,
        borderRadius: 10,
        marginVertical: 5,
    },
    registerButtonText: {
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    registerButton: {
        marginVertical: 30,
        backgroundColor: '#FF5E59',
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
        width: 250,
    },
    loginButtonText: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    loginButton: {
        paddingVertical: 5,
    }
})