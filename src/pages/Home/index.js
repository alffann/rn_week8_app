import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import {connect} from 'react-redux'
import {updateStatusLogin} from '../../redux/actions/LoginAction'
import {BookList} from '../../redux/actions/HomeAction'
import {styles} from './style'

export class Home extends Component {
    async componentDidMount(){
        await this.props.updateStatusLogin()
        this.props.BookList()
    }

    render() {
        return (
            <ScrollView>
            <View>
                <Text style={styles.greetingText}> Hallo {this.props.dataLogin.name}</Text>

                <Text style={styles.sectionText}> Recommended </Text>
                <ScrollView horizontal={true}>
                    <View style={styles.recBookListContainer}>
                        {this.props.dataBook.listBook.map(book => (
                            <TouchableOpacity key={book.id}
                                onPress={()=>{
                                    this.props.navigation.navigate('BookDetail', {bookId: book.id})
                                }}                                        
                            >
                                <View style={styles.recBookContainer}>
                                    <Image style={styles.recBookImage}
                                        source={{uri: book.cover_image}}
                                    />
                                    <Text style={styles.recBookTitle}>{book.title}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>

                <Text style={styles.sectionText}> Popular Book </Text>
                <View style={styles.popBookListContainer}>
                    {this.props.dataBook.listBook.map(book => (
                        <TouchableOpacity key={book.id}
                            onPress={()=>{
                                this.props.navigation.navigate('BookDetail', {BookId: book.id})
                            }}                                        
                            style={styles.popBookContainer}
                        >
                            <Image style={styles.popBookImage}
                                source={{uri: book.cover_image}}
                            />
                            <Text style={styles.popBookTitle}>{book.title}</Text>

                        </TouchableOpacity>
                    ))}
                </View>
            </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => ({
    dataLogin: state.LoginReducer,
    dataBook: state.HomeReducer
})
  
const mapDispatchToProps = {
    updateStatusLogin,
    BookList
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Home)
