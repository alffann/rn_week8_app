import Home from './Home'
import Search from './Search'
import Favorite from './Favorite'

import Splash from './Splash'
import Login from './Login'
import Register from './Register'
import SuccessReg from './SuccessReg'
import BookDetail from './BookDetail'

export {
    Home, 
    Search, 
    Favorite, 
    Splash, 
    Login, 
    Register, 
    SuccessReg, 
    BookDetail
}