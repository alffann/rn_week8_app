import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 40,
    },
    errorView: {
        marginVertical: 20,
        backgroundColor: 'red',
        padding: 5,
        borderRadius: 5,
        width: 280,
    },
    errorText: {
        fontSize: 15,
        textAlign: 'center',
        color: 'white',
    },
    inputData: {
        padding: 5,
        backgroundColor: 'white',
        color: 'black',
        width: 350,
        borderRadius: 10,
        marginVertical: 5,
    },
    loginButtonText: {
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    loginButton: {
        marginVertical: 30,
        backgroundColor: 'green',
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
        width: 350,
    },
    registerButtonText: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    registerButton: {
        paddingVertical: 5,
    }
})