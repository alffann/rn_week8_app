import {
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    STATUS_LOGIN,
} from './../actions/LoginAction'

// Mendefinisikan awal state
const StateAwal = {
    name: null,
    isLoginSuccess: false,
    isLoading: false,
    errorMessage: null,
}

export default (state = StateAwal, action) => {
    switch (action.type) {
        case LOGIN_START:
            return { state }
        case LOGIN_SUCCESS:
            return { ...state, isLoginSuccess: true, errorMessage: null, name: action.name}
        case LOGIN_FAILED:
            return { ...state, errorMessage: action.errorMessage}
        case STATUS_LOGIN:
            return { ...state, isLoginSuccess: false}

    default:
        return state
    }
}
