import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

export const BOOK_LIST_START = 'BOOK_LIST_START';
export const BOOK_LIST_SUCCESS = 'BOOK_LIST_SUCCESS';
export const BOOK_LIST_FAILED = 'BOOK_LIST_FAILED';

export const BookList = () => async dispatch => {
    try{
        dispatch({type: BOOK_LIST_START})
        const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        const hasilFetchingTotal = await axios.get('http://code.aldipee.com/api/v1/books', {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )
        const totalResults = hasilFetchingTotal.data.totalResults

        const hasilFetching = await axios.get(`http://code.aldipee.com/api/v1/books?limit=${totalResults}`, {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )

        if (hasilFetching.status === 200) {
            dispatch({
                type: BOOK_LIST_SUCCESS,
                dataBookList: hasilFetching.data.results,
            })
        }
    } catch(error) {
        dispatch({
            type: BOOK_LIST_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}