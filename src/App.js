import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import { Provider } from 'react-redux'
import { bookStore } from './redux/store';

export class App extends Component {
  render() {
    return (
      <Provider store={bookStore}>
        <NavigationContainer>
          <Router />
        </NavigationContainer>    
      </Provider>
    )
  }
}

export default App
